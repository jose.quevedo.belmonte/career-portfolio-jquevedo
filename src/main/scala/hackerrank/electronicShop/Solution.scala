// https://www.hackerrank.com/challenges/electronics-shop/problem //

package hackerrank.electronicShop

import scala.annotation.tailrec

object Solution extends App {
  val std = scala.io.StdIn
  val bnm = std.readLine().split(" ").map(_.toInt)
  val b = bnm.head
  val n = bnm(1)
  val m = bnm(2)
  val keyboards = std.readLine().split(" ").map(_.toInt).toList
  val usbs = std.readLine().split(" ").map(_.toInt).toList


  keyboards.sorted
  usbs.sorted

  def calculate(xs1: List[Int], xs2: List[Int], b: Int): Int = {

    @tailrec
    def go(xs1: List[Int], xs2: List[Int], b: Int, acc: List[Int]): List[Int] = {
      xs1 match {
        case Nil => acc
        case h :: t => go(t, xs2, b, xs2.map(_ + h).filter(_ <= b) ++ acc)
      }
    }

    val sol = go(xs1, xs2, b, Nil)
    if (sol == Nil)
      -1
    else
      sol.max
  }

  if (keyboards.size > usbs.size)
    println(calculate(usbs, keyboards, b))
  else
    println(calculate(keyboards, usbs, b))
}
