// https://www.hackerrank.com/challenges/non-divisible-subset/problem //

package hackerrank.nonDivisibleSubset

import scala.annotation.tailrec

object Solution extends App {

  val nk = scala.io.StdIn.readLine().split(" ")

  val n = nk(0).toInt

  val k = nk(1).toInt

  val s = scala.io.StdIn.readLine().split(" ").map(_.trim.toInt).toList


  def createSubSets(l: List[Int]): List[List[Int]] = {
    l.toSet[Int].subsets.map(_.toList.sorted).toList.tail.reverse
  }

  def obtainSubset(l: List[Int], k: Int): Int = {
    @tailrec
    def go(xs: Set[Int], aux: Set[Int], isDivisible: Boolean, k: Int, sol: Int): Int = {

      xs match {
        case _ if isDivisible => 1
        case x if x.isEmpty =>
          sol
        case x =>
          go(x.tail, aux.tail, aux.tail.map(_ + aux.head).count(_ % k == 0) != 0, k, sol)
        //go(t, aux.tail :+ aux.head, if (aux.tail.map(_ + aux.head).filter(_ % k != 0) == Nil) acc - 1 else acc, k)
      }
    }

    go(l.toSet, l.toSet, isDivisible = false, k, l.length)
  }
  @tailrec
  def rec(xs: List[List[Int]], k: Int, acc: Int): Int = {
    xs match {
      case _ if acc != 1 => acc
      case Nil => 1
      case h :: t => rec(t, k, obtainSubset(h, k))
    }
  }

  println(rec(createSubSets(s), k, 1))

}
