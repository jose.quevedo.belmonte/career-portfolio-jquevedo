// https://www.hackerrank.com/challenges/apple-and-orange/problem //

package hackerrank.appleAndOrange

object Solution {

  def main(args: Array[String]) {
    val stdin = scala.io.StdIn

    val st = stdin.readLine.split(" ")

    val s = st(0).trim.toInt

    val t = st(1).trim.toInt

    val ab = stdin.readLine.split(" ")

    val a = ab(0).trim.toInt

    val b = ab(1).trim.toInt

    val mn = stdin.readLine.split(" ")

    val m = mn(0).trim.toInt

    val n = mn(1).trim.toInt

    val apples: List[Int] = stdin.readLine.split(" ").map(_.trim.toInt).toList


    val oranges: List[Int] = stdin.readLine.split(" ").map(_.trim.toInt).toList
    countApplesAndOranges(s, t, a, b, apples, oranges)
  }

  // Complete the countApplesAndOranges function below.
  def countApplesAndOranges(s: Int, t: Int, a: Int, b: Int, apples: List[Int], oranges: List[Int]) {

    val positionApples = apples.map(x => a + x)
    val positionOranges = oranges.map(x => b + x)


    println(positionApples.count(x => x >= s && x <= t))
    println(positionOranges.count(x => x >= s && x <= t))

  }
}
