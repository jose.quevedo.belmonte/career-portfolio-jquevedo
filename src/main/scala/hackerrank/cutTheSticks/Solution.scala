// https://www.hackerrank.com/challenges/cut-the-sticks/problem //

package hackerrank.cutTheSticks

import scala.annotation.tailrec

object Solution extends App {
  val n = scala.io.StdIn.readLine

  val sticksLength: List[Int] = scala.io.StdIn.readLine.split(" ").map(_.trim.toInt).toList

  def cutSticks(xs: List[Int]): List[Int] = {
    @tailrec
    def go(xs: List[Int], sol: List[Int]): List[Int] = {
      xs match {
        case h :: t if h::t == h :: Nil || h == t.last =>
          sol :+ xs.length
        case h :: _ =>
          go(xs.map(_ - h).filter(_ != 0), sol :+ xs.length)
      }
    }
    go(xs.sorted, Nil)
  }

  cutSticks(sticksLength).foreach(println)
}
