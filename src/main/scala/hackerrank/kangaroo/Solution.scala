package hackerrank.kangaroo

// https://www.hackerrank.com/challenges/kangaroo/problem //

object Solution extends App {

  val st = scala.io.StdIn.readLine.split(" ")

  val x1 = st(0).trim.toInt

  val v1 = st(1).trim.toInt

  val x2 = st(2).trim.toInt

  val v2 = st(3).trim.toInt


  println(func(x1, v1, x2, v2))

  def func(x1: Int, v1: Int, x2: Int, v2: Int): String = {
    if ((v1 >= v2 && x1 > x2) || (v2 >= v1 && x2 > x1))
      s"NO"
    else if ((x2 - x1) % (v1 - v2) == 0) {
      s"YES"
    }
    else
      s"NO"
  }
}


