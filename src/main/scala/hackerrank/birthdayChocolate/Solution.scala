// https://www.hackerrank.com/challenges/the-birthday-bar/problem //

package hackerrank.birthdayChocolate

import scala.annotation.tailrec

object Solution extends App {
  val n = scala.io.StdIn.readLine().split(" ")

  val s = scala.io.StdIn.readLine().split(" ").map(_.trim.toInt).toList

  val dm = scala.io.StdIn.readLine().split(" ")

  val d = dm(0).toInt

  val m = dm(1).toInt

  @tailrec
  def cutChocolate(xs: List[Int], goal: Int, m: Int, sol: Int): Int = {
    var variable = sol
    xs match {
      case x if xs.length < m => sol
      case x =>
        if (x.slice(0, m).sum == goal)
          cutChocolate(x.tail, goal, m, sol + 1)
        else
          cutChocolate(x.tail, goal, m, sol)
    }
  }

  println(cutChocolate(s, d, m, 0))
}
