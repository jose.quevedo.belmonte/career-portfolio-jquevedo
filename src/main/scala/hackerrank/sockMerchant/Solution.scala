package hackerrank.sockMerchant

import scala.annotation.tailrec

//https://www.hackerrank.com/challenges/sock-merchant/problem
object Solution extends App {
  val n = scala.io.StdIn.readLine()
  val s = scala.io.StdIn.readLine().split(" ").map(_.toInt).toList


  def obtainPairs(l: List[Int]): Int = {
    @tailrec
    def go(xs: List[Int], acc: Int): Int = {
      xs match {
        case h::h1::t => if (h == h1) go(t,acc+1) else go(h1+:t,acc)
        case _ => acc

      }
    }
    go(l, 0)
  }

  println(obtainPairs(s.sorted))
}