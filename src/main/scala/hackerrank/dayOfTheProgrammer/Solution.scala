// https://www.hackerrank.com/challenges/day-of-the-programmer/problem //

package hackerrank.dayOfTheProgrammer

object Solution {

  def main(args: Array[String]) {

    val year = scala.io.StdIn.readLine.trim.toInt

    val result = dayOfProgrammer(year)

    println(result)
  }

  // Complete the dayOfProgrammer function below.
  def dayOfProgrammer(year: Int): String = {

    year match {
      case x if x == 1918 => s"26.09.$year"
      case x if (year <= 1917 && year % 4 == 0) || ((year > 1918) & (year % 400 == 0 || ((year % 4 == 0) && (year % 100 != 0)))) => s"12.09.$year"
      case _ => s"13.09.$year"
    }
  }
}