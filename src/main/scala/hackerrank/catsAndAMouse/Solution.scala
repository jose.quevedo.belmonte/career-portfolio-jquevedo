//https://www.hackerrank.com/challenges/cats-and-a-mouse/problem

package hackerrank.catsAndAMouse

object Solution extends App{

  val n = scala.io.StdIn.readLine().toInt

  for(i <- 1 to n) {
    val s = scala.io.StdIn.readLine().split(" ").map(_.toInt).toList
    println(catAndMouse(s.head,s(1),s(2)))
  }

  def catAndMouse (catA:Int,catB:Int,mouse:Int): String ={
    if (Math.abs(catA-mouse)<Math.abs(catB-mouse))
      "Cat A"
    else if (Math.abs(catA-mouse)>Math.abs(catB-mouse))
      "Cat B"
    else
      "Mouse C"
  }
}
