// https://www.hackerrank.com/challenges/migratory-birds/problem //

package hackerrank.migratoryBirds

object Solution extends App{
  val n = scala.io.StdIn.readLine()
  val s = scala.io.StdIn.readLine().split(" ").map(_.trim.toInt).toList

  val sol = s.groupBy(z=> z).mapValues(_.size).maxBy(x => x._2)

  println(sol._1)
}
