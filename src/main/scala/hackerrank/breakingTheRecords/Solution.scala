// https://www.hackerrank.com/challenges/breaking-best-and-worst-records/problem //

package hackerrank.breakingTheRecords

import scala.annotation.tailrec

object Solution {

  def main(args: Array[String]) {
    val stdin = scala.io.StdIn


    val n = stdin.readLine.trim.toInt

    val scores = stdin.readLine.split(" ").map(_.trim.toInt).toList
    val result = breakingRecords(scores)
    println(s"${result.head} ${result(1)}")

  }

  // Complete the breakingRecords function below.
  def breakingRecords(scores: List[Int]): List[Int] = {
    @tailrec
    def fun(xs:List[Int],min:Int,max:Int,accMin:Int,accMax:Int): List[Int] ={
      xs match {
        case Nil => List(accMax, accMin)
        case h::t if h>max => fun(t,min,h,accMin,accMax+1)
        case h::t if h<min => fun(t,h,max,accMin+1,accMax)
        case _::t => fun(t,min,max,accMin,accMax)
      }
    }
    fun(scores,scores.head,scores.head,0,0)
  }
}
