package hackerrank.gradingStudents

//https://www.hackerrank.com/challenges/grading/problem
object Solution extends App {
  val std = scala.io.StdIn
  val n = std.readLine().toInt
  for (_ <- 1 to n) {
    val m = std.readLine().toInt
    val multM = obtainNextMultiple(m)
    if (multM - m < 3 && m >= 38)
      println(multM)

    else
      println(m)
  }

  def obtainNextMultiple(x: Int): Int = {
    5 - x % 5 + x
  }
}
