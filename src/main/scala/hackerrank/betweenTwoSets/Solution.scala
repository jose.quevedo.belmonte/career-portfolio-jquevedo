// https://www.hackerrank.com/challenges/between-two-sets/problem //

package hackerrank.betweenTwoSets

import scala.annotation.tailrec

object Solution extends App {
  val std = scala.io.StdIn
  val nm = std.readLine().split(" ")

  val a = std.readLine().split(" ").map(_.toInt).toList
  val b = std.readLine().split(" ").map(_.toInt).toList

  println(conteo(a++b.sorted))
  def conteo(xs:List[Int]):Int ={
    println(xs)

    @tailrec
    def rec(xs:List[Int],acc :Int): Int ={
      xs match {
        case Nil => acc
        case h::t =>
          println(s"$h $t")
          println(t.count(_%(h*h)==0)!=0)

        rec(t, if (t.count(_%(h*h)==0)!=0)  acc +1 else acc)
      }
    }
    rec(xs,0)
  }
}
